var gulp = require('gulp');
var fontmin = require('gulp-fontmin');

gulp.task('font', function() {
    return gulp.src('./fonts/*.ttf')
        .pipe(fontmin({
            quiet: true
        }))
        .pipe(gulp.dest('./converted-fonts'));
});