# Font converter
----------

### Installation
Run `npm install` to install all packages.

### Use
Run `gulp font` to convert all fonts in the `/fonts` folder.
They will be converted to the `/converted-fonts` folder.